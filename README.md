# InspecTux

UI Automation for RPA and automated UI testing, for Linux.

## Recommended setup for UI tests

InspecTux is designed to be used with the testing environment provided by Go.
This means, a UI test is a regular Go test function - or may use any of the available libraries for organizing tests.

To prevent running UI tests every time unit tests are run, this is the recommended setup:

* Keep your UI tests in a separate package, e.g. `ui_test`
* In each test function, call `uitesting.UiTest(t)`  
This will prevent running the test case unless the environment variable "RUN_UI_TESTS" is set to a non-blank value.  
So, when running unit tests, the UI tests will be skipped.
* To run the UI tests, run all tests from the ui test directory.  
We recommend disabling timeouts, as UI tests may run longer than the default timeout 10 minutes.  
`-count=1` is used to force running the tests, essentially skipping the cache for test results.  
`RUN_UI_TESTS=1 go test -count=1 -v -timeout 0 ./ui_test/...`

If you use Visual Studio Code, you can configure a test task in `.vscode/tasks.json` like this:

```json
{
	"version": "2.0.0",
	"tasks": [
		{
			"label": "Run UI Tests",
			"command": "go",
			"type": "process",
			"options": {
				"env": {
					"RUN_UI_TESTS": "true",
				},
			},
			"args": [
				"test",
				"-v",
				"-count=1",
				"-timeout",
				"0",
				"./ui_test/..."
			],
			"problemMatcher": [
				"$go"
			],
			"presentation": {
				"reveal": "always"
			},
			"group": "test"
		},
	]
}
```

If you use Makefiles, add a phony target to run UI tests:

```makefile
.PHONY uitests
uitests:
	RUN_UI_TESTS=1 go test -v -timeout 0 ./ui_test/...
```

Similarly, you can run the UI tests from continuous integration. Note that the UI tests need a graphical environment, e.g. using `xvfb-run`.
