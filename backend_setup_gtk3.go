package inspectux

import (
	"fmt"
	"log"
	"time"

	"github.com/godbus/dbus/v5"
	"gitlab.com/cubiest/inspectux/introspection"
)

var (
	// Ensure GTK3BackendSetup can set up an Introspection backend
	_ BackendSetup[Backend] = &GTK3BackendSetup{}
)

type GTK3BackendSetup struct{}

// Create a new introspection backend, can be used with AppHandle.
func NewGTK3Backend() *GTK3BackendSetup {
	return &GTK3BackendSetup{}
}

func (b *GTK3BackendSetup) ConfigureEnv() []string {
	// TODO: Handle appending to existing GTK_MODULES environment
	return []string{
		"GTK_MODULES=inspectux_introspection_gtk3",
	}
}

func (b *GTK3BackendSetup) AttachToPID(pid int) (Backend, error) {
	log.Println("Connection introspection backend")
	// TODO: Use retry logic instead of sleep
	time.Sleep(time.Second * 3)

	sessionBus, err := dbus.SessionBus()
	if err != nil {
		return nil, fmt.Errorf("error connecting to session bus: %w", err)
	}

	backend := &IntrospectionBackend{
		remote: introspection.NewIntrospectionserver(sessionBus.Object(fmt.Sprint(introspection.ServiceNamePrefix, pid), dbus.ObjectPath("/"))),
	}

	err = backend.Check()
	if err != nil {
		return nil, err
	}

	log.Println("Introspection backend connected")
	return backend, nil
}
