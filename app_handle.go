package inspectux

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"syscall"
)

var (
	ErrNoBackend = fmt.Errorf("no backend configured")
)

type AppHandle struct {
	pid int

	process *os.Process

	backend Backend
	input   Input
	root    *Element
}

func (h *AppHandle) Kill() error {
	return h.process.Kill()
}

func (h *AppHandle) Root() *Element {
	return h.root
}

type commonSetup struct {
	introspection BackendSetup[Backend]
	input         BackendSetup[Input]
}

type AppHandleSetup struct {
	commonSetup
}

type AppHandleStart struct {
	commonSetup

	name string
	args []string

	env              []string
	workingDirectory string

	cmd *exec.Cmd
}

type AppHandleAttachPID struct {
	commonSetup

	pid int
}

func NewAppHandle(backend BackendSetup[Backend], input BackendSetup[Input]) *AppHandleSetup {
	return &AppHandleSetup{
		commonSetup{
			introspection: backend,
			input:         input,
		},
	}
}

func (h *AppHandleSetup) WithCommand(name string, args ...string) *AppHandleStart {
	return &AppHandleStart{
		commonSetup: h.commonSetup,
		name:        name,
		args:        args,
		env:         make([]string, 0),
	}
}

func (h *AppHandleStart) WithParentEnv() *AppHandleStart {
	h.env = append(h.env, os.Environ()...)
	return h
}

func (h *AppHandleStart) WithEnv(name, value string) *AppHandleStart {
	h.env = append(h.env, fmt.Sprintf("%s=%s", name, value))
	return h
}

func (h *AppHandleStart) WithWorkingDirectory(path string) *AppHandleStart {
	h.workingDirectory = path
	return h
}

func (h *AppHandleStart) Start() (*AppHandle, error) {

	h.cmd = exec.Command(h.name, h.args...)
	h.cmd.Env = h.env

	h.cmd.Env = append(h.cmd.Env, h.introspection.ConfigureEnv()...)
	h.cmd.Env = append(h.cmd.Env, h.input.ConfigureEnv()...)

	h.cmd.Dir = h.workingDirectory
	h.cmd.Stderr = os.Stderr
	h.cmd.Stdout = os.Stdout

	err := h.cmd.Start()
	if err != nil {
		return nil, fmt.Errorf("error starting process: %w", err)
	}

	// TODO: Extract this common part, with config option whether to kill process on error

	handle := &AppHandle{
		pid:     h.cmd.Process.Pid,
		process: h.cmd.Process,
	}

	handle.backend, err = h.introspection.AttachToPID(handle.pid)
	if err != nil {
		// TODO: Handle this better (allow caller to decide)
		handle.Kill()
		return nil, fmt.Errorf("error attaching introspection backend: %w", err)
	}

	handle.input, err = h.input.AttachToPID(handle.pid)
	if err != nil {
		// TODO: Handle this better (allow caller to decide)
		handle.Kill()
		return nil, fmt.Errorf("error attaching input backend: %w", err)
	}

	handle.root = &Element{
		scope:   "/",
		backend: handle.backend,
		input:   handle.input,
	}

	log.Println("AppHandle ready")

	return handle, nil
}

func (h *AppHandleSetup) WithPID(pid int) *AppHandleAttachPID {
	return &AppHandleAttachPID{
		commonSetup: h.commonSetup,
		pid:         pid,
	}
}

func (h *AppHandleAttachPID) Attach() (*AppHandleAttachPID, error) {
	var err error
	process, err := os.FindProcess(h.pid)
	if err != nil {
		return nil, fmt.Errorf("error attaching process: %w", err)
	}

	err = process.Signal(syscall.Signal(0))
	if err != nil {
		return nil, fmt.Errorf("error attaching process: %w", err)
	}

	handle := &AppHandle{
		pid:     h.pid,
		process: process,
	}

	handle.backend, err = h.introspection.AttachToPID(handle.pid)
	if err != nil {
		return nil, fmt.Errorf("error attaching introspection backend: %w", err)
	}

	handle.input, err = h.input.AttachToPID(handle.pid)
	if err != nil {
		return nil, fmt.Errorf("error attaching input backend: %w", err)
	}

	handle.root = &Element{
		scope:   "/",
		backend: handle.backend,
		input:   handle.input,
	}

	log.Println("AppHandle ready")

	return h, nil
}
