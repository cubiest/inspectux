package ui_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cubiest/inspectux"
	"gitlab.com/cubiest/inspectux/uitesting"
)

func TestUIExample(t *testing.T) {
	uitesting.UiTest(t)

	tempDir := t.TempDir()

	// TODO: Configure XDG env vars ()
	handle, err := inspectux.NewAppHandle(inspectux.NewGTK3Backend(), inspectux.NewUinputBackend()).
		WithCommand("gedit").
		WithParentEnv().
		WithWorkingDirectory(tempDir).
		Start()
	if err != nil {
		t.Fatal(err)
	}
	defer handle.Kill()

	attributes := handle.Root().GetAttributes().OrFail(t)
	assert.Empty(t, attributes)

	menuButton := handle.Root().
		GetChild("/GeditWindow/GtkPaned/GeditHeaderBar/GtkMenuButton").
		OrFail(t)

	time.Sleep(5 * time.Second)

	// Type "Z"
	handle.Root().KeyDown(44).OrFail(t)
	handle.Root().KeyUp(44).OrFail(t)

	menuButton.Click(0).OrFail(t)

	time.Sleep(5 * time.Second)
}
