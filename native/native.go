package native

import "context"

const (
	ServiceNameNativeInput = "com.cubiest.inspectux.native.input"
)

type InputRemote interface {
	MoveMouse(ctx context.Context, x int32, y int32, absolute bool) (err error)
	Click(ctx context.Context, button uint8) (err error)

	Type(ctx context.Context, text string) (err error)
	KeyDown(ctx context.Context, key uint16) (err error)
	KeyUp(ctx context.Context, key uint16) (err error)

	GetAPIVersion(ctx context.Context) (version string, err error)
	GetCapabilities(ctx context.Context) (capabilities []string, err error)
}

type MouseButton uint8

const (
	MouseButtonLeft MouseButton = iota
	MouseButtonRight
	MouseButtonMiddle
	MouseButtonBack
	MouseButtonForward
)
