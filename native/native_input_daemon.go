// Code generated by dbus-codegen-go DO NOT EDIT.
package native

import (
	"context"

	"github.com/godbus/dbus/v5"
)

// Interface name constants.
const (
	InterfaceNativeInput = "com.cubiest.inspectux.native.input"
)

// NewNativeInput creates and allocates com.cubiest.inspectux.native.input.
func NewNativeInput(object dbus.BusObject) *NativeInput {
	return &NativeInput{object}
}

// NativeInput implements com.cubiest.inspectux.native.input D-Bus interface.
type NativeInput struct {
	object dbus.BusObject
}

// MoveMouse calls com.cubiest.inspectux.native.input.MoveMouse method.
//
// Annotations:
//
//	@org.gtk.GDBus.DocString = Move the mouse cursor either relative to the current position, or absolute.
func (o *NativeInput) MoveMouse(ctx context.Context, x int32, y int32, absolute bool) (err error) {
	err = o.object.CallWithContext(ctx, InterfaceNativeInput+".MoveMouse", 0, x, y, absolute).Store()
	return
}

// Click calls com.cubiest.inspectux.native.input.Click method.
//
// Annotations:
//
//	@org.gtk.GDBus.DocString = Click a mouse button.
func (o *NativeInput) Click(ctx context.Context, button byte) (err error) {
	err = o.object.CallWithContext(ctx, InterfaceNativeInput+".Click", 0, button).Store()
	return
}

// Type calls com.cubiest.inspectux.native.input.Type method.
//
// Annotations:
//
//	@org.gtk.GDBus.DocString = Type a string of characters. Automatically handles modifiers and key down/up.
func (o *NativeInput) Type(ctx context.Context, text string) (err error) {
	err = o.object.CallWithContext(ctx, InterfaceNativeInput+".Type", 0, text).Store()
	return
}

// KeyDown calls com.cubiest.inspectux.native.input.KeyDown method.
//
// Annotations:
//
//	@org.gtk.GDBus.DocString = Presses a key without releasing it. Argument is the key code to press down. Use Linux Input Event codes (linux/input-event-codes.h KEY_*).
func (o *NativeInput) KeyDown(ctx context.Context, key uint16) (err error) {
	err = o.object.CallWithContext(ctx, InterfaceNativeInput+".KeyDown", 0, key).Store()
	return
}

// KeyUp calls com.cubiest.inspectux.native.input.KeyUp method.
//
// Annotations:
//
//	@org.gtk.GDBus.DocString = Releases a key which was (presumably) previously pressed. Argument is the key code to release. Use Linux Input Event codes (linux/input-event-codes.h KEY_*).
func (o *NativeInput) KeyUp(ctx context.Context, key uint16) (err error) {
	err = o.object.CallWithContext(ctx, InterfaceNativeInput+".KeyUp", 0, key).Store()
	return
}

// GetAPIVersion calls com.cubiest.inspectux.native.input.GetAPIVersion method.
//
// Annotations:
//
//	@org.gtk.GDBus.DocString = Get the version of the API this input server uses. This describes available methods in DBus, not the available attributes of any objects.
func (o *NativeInput) GetAPIVersion(ctx context.Context) (version string, err error) {
	err = o.object.CallWithContext(ctx, InterfaceNativeInput+".GetAPIVersion", 0).Store(&version)
	return
}

// GetCapabilities calls com.cubiest.inspectux.native.input.GetCapabilities method.
//
// Annotations:
//
//	@org.gtk.GDBus.DocString = Get the capabilities of this input server. Capabilities are server-defined and may be used to check if a server supports a required feature.
func (o *NativeInput) GetCapabilities(ctx context.Context) (capabilities []string, err error) {
	err = o.object.CallWithContext(ctx, InterfaceNativeInput+".GetCapabilities", 0).Store(&capabilities)
	return
}
