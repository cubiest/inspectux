package inspectux

import (
	"context"
	"fmt"
	"path"

	"gitlab.com/cubiest/inspectux/introspection"
)

var (
	// Ensure IntrospectionBackend implements Backend interface
	_ Backend = &IntrospectionBackend{}
)

type IntrospectionBackend struct {
	remote introspection.IntrospectionserverRemote
}

// Kind implements Backend.
func (b *IntrospectionBackend) Kind() string {
	// TODO: This should be provided via dbus
	return "GTK3 Introspection"
}

// Create a new introspection backend with an existing remote.
// Not compatible with AppHandle.
func NewIntrospectionBackendWithRemote(remote introspection.IntrospectionserverRemote) *IntrospectionBackend {
	return &IntrospectionBackend{
		remote: remote,
	}
}

func (b *IntrospectionBackend) Check() error {
	// TODO: Check if API Version meets minimal version
	_, err := b.remote.GetAPIVersion(context.Background())
	if err != nil {
		return fmt.Errorf("error checking introspection capabilities: %w", err)
	}

	return nil
}

func (b *IntrospectionBackend) GetChildren(elementPath string) ([]*BackendNode, error) {

	r, err := b.remote.GetChildren(context.Background(), elementPath)
	if err != nil {
		return nil, fmt.Errorf("error querying children of \"%s\": %w", elementPath, err)
	}
	// TODO: Translate return code to better error
	if r.V0 != 0 {
		return nil, fmt.Errorf("error querying children of \"%s\": RC %d", elementPath, r.V0)
	}
	nodes := make([]*BackendNode, 0, len(r.V1))
	for _, v := range r.V1 {
		nodes = append(nodes, &BackendNode{
			Path:  path.Join(elementPath, v.V0),
			Name:  v.V0,
			Class: v.V1,
		})
	}
	return nodes, nil
}

func (b *IntrospectionBackend) GetElement(elementPath string) (*BackendNode, error) {

	r, err := b.remote.GetElement(context.Background(), elementPath)
	if err != nil {
		return nil, fmt.Errorf("error querying element at \"%s\": %w", elementPath, err)
	}
	// TODO: Translate return code to better error
	if r.V0 != 0 {
		return nil, fmt.Errorf("error querying element at \"%s\": RC %d", elementPath, r.V0)
	}
	return &BackendNode{
		Path:  elementPath,
		Name:  r.V1,
		Class: r.V2,
	}, nil
}

func (b *IntrospectionBackend) GetAttributes(elementPath string) ([]*Attribute, error) {

	r, err := b.remote.GetAttributes(context.Background(), elementPath)
	if err != nil {
		return nil, fmt.Errorf("error querying attributes of \"%s\": %w", elementPath, err)
	}
	// TODO: Translate return code to better error
	if r.V0 != 0 {
		return nil, fmt.Errorf("error querying attributes of \"%s\": RC %d", elementPath, r.V0)
	}

	attributes := make([]*Attribute, 0, len(r.V1))
	for _, v := range r.V1 {
		attributes = append(attributes, &Attribute{
			Name:  v.V0,
			Type:  v.V1.Signature().String(), // TODO: Add remote type to dbus interface, so we can display it here
			Value: v.V1.Value(),
		})
	}
	return attributes, nil
}

func (b *IntrospectionBackend) GetRect(elementPath string, global bool) (*Rect, error) {

	r, err := b.remote.GetRect(context.Background(), elementPath, global)
	if err != nil {
		return nil, fmt.Errorf("error querying rect of \"%s\": %w", elementPath, err)
	}
	// TODO: Translate return code to better error
	if r.V0 != 0 {
		return nil, fmt.Errorf("error querying rect of \"%s\": RC %d", elementPath, r.V0)
	}

	return &Rect{
		X: r.V1,
		Y: r.V2,
		W: r.V3,
		H: r.V4,
	}, nil
}
