module gitlab.com/cubiest/inspectux

go 1.22.2

require (
	github.com/amenzhinsky/dbus-codegen-go v0.2.0
	github.com/godbus/dbus/v5 v5.1.0
	github.com/stretchr/testify v1.9.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
