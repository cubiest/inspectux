package uitesting

import (
	"os"
	"testing"
)

func UiTest(t *testing.T) {
	t.Helper()

	runUiTests := os.Getenv("RUN_UI_TESTS")
	if runUiTests == "" {
		t.Skip("this is a UI test - set env RUN_UI_TESTS to run this test")
	}
}
