package inspectux

type ValueResult[T any] struct {
	e   T
	err error
}

func (r ValueResult[T]) OrFail(t Failable) T {
	if r.err != nil {
		var def T
		t.Error(r.err)
		return def
	}
	return r.e
}

func (r ValueResult[T]) OrPanic() T {
	if r.err != nil {
		panic(r.err)
	}
	return r.e
}

func (r ValueResult[T]) OrErr() (T, error) {
	if r.err != nil {
		var def T
		return def, r.err
	}
	return r.e, nil
}

func newValueResult[T any](e T, err error) ValueResult[T] {
	return ValueResult[T]{
		e:   e,
		err: err,
	}
}

type Result struct {
	err error
}

func (r Result) OrFail(t Failable) {
	if r.err != nil {
		t.Error(r.err)
	}
}

func (r Result) OrPanic() {
	if r.err != nil {
		panic(r.err)
	}
}

func (r Result) OrErr() error {
	if r.err != nil {
		return r.err
	}
	return nil
}

func newResult(err error) Result {
	return Result{
		err: err,
	}
}
