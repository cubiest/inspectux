package inspectux

import (
	"log"
	"path"
	"time"

	"gitlab.com/cubiest/inspectux/native"
)

var DefaultTimeout time.Duration = time.Second * 10
var DefaultInterval time.Duration = time.Millisecond * 500

var (
	// Ensure implementations of ElementAccess
	_ ElementAccess           = &Element{}
	_ ElementAccess           = &elementAccessTimeout{}
	_ PositionalElementAccess = &positionalAccess{}
)

// Interface matching testing.T for Error()
type Failable interface {
	Error(args ...any)
}

type ElementAccess interface {
	GetChild(relativePath string) ValueResult[*Element]
	GetChildAttributes(relativePath string) ValueResult[[]*Attribute]
	GetAttributes() ValueResult[[]*Attribute]

	AtCenter() PositionalElementAccess
	At(x, y uint32) PositionalElementAccess
	GetRect() ValueResult[Rect]

	// TODO: Mouse button constants from native package
	Click(button int) Result

	Type(text string) Result
	KeyDown(key uint16) Result
	KeyUp(key uint16) Result
}

type PositionalElementAccess interface {
	Click(button int) Result
}
type Element struct {
	scope   string
	backend Backend
	input   Input

	node *BackendNode
}

func (e *Element) WithTimeout(timeout time.Duration) ElementAccess {
	return &elementAccessTimeout{
		e:       e,
		timeout: timeout,
	}
}

// Convenience shortcuts using default timeout:

func (e *Element) At(x, y uint32) PositionalElementAccess {
	return e.WithTimeout(DefaultTimeout).At(x, y)
}
func (e *Element) AtCenter() PositionalElementAccess {
	return e.WithTimeout(DefaultTimeout).AtCenter()
}
func (e *Element) Click(button int) Result {
	return e.WithTimeout(DefaultTimeout).Click(button)
}
func (e *Element) GetRect() ValueResult[Rect] {
	return e.WithTimeout(DefaultTimeout).GetRect()
}
func (e *Element) GetChild(relativePath string) ValueResult[*Element] {
	return e.WithTimeout(DefaultTimeout).GetChild(relativePath)
}
func (e *Element) GetChildAttributes(relativePath string) ValueResult[[]*Attribute] {
	return e.WithTimeout(DefaultTimeout).GetChildAttributes(relativePath)
}
func (e *Element) GetAttributes() ValueResult[[]*Attribute] {
	return e.WithTimeout(DefaultTimeout).GetAttributes()
}
func (e *Element) Type(text string) Result {
	return e.WithTimeout(DefaultTimeout).Type(text)
}
func (e *Element) KeyDown(key uint16) Result {
	return e.WithTimeout(DefaultTimeout).KeyDown(key)
}
func (e *Element) KeyUp(key uint16) Result {
	return e.WithTimeout(DefaultTimeout).KeyUp(key)
}

type elementAccessTimeout struct {
	e        *Element
	timeout  time.Duration
	interval time.Duration
}

func (ea *elementAccessTimeout) At(x, y uint32) PositionalElementAccess {
	center, err := doWithRetry(func() (Point, error) {
		rect, err := ea.e.backend.GetRect(ea.e.scope, true)
		if err != nil {
			return Point{}, err
		}
		return rect.Min().Add(Point{X: x, Y: y}), nil
	}, ea.timeout, ea.interval)
	return &positionalAccess{
		e:        ea.e,
		timeout:  ea.timeout,
		interval: ea.interval,
		point:    center,
		err:      err,
	}
}

func (ea *elementAccessTimeout) AtCenter() PositionalElementAccess {
	center, err := doWithRetry(func() (Point, error) {
		rect, err := ea.e.backend.GetRect(ea.e.scope, true)
		if err != nil {
			return Point{}, err
		}
		return rect.Center(), nil
	}, ea.timeout, ea.interval)
	return &positionalAccess{
		e:        ea.e,
		timeout:  ea.timeout,
		interval: ea.interval,
		point:    center,
		err:      err,
	}
}

func (ea *elementAccessTimeout) Click(button int) Result {
	return ea.AtCenter().Click(button)
}

func (ea *elementAccessTimeout) GetRect() ValueResult[Rect] {
	return newValueResult(doWithRetry(func() (Rect, error) {
		rect, err := ea.e.backend.GetRect(ea.e.scope, true)
		if err != nil {
			return Rect{}, err
		}
		return *rect, nil
	}, ea.timeout, ea.interval))
}

func (ea *elementAccessTimeout) Type(text string) Result {
	return newResult(doWithRetryNV(func() error {
		return ea.e.input.Type(text)
	}, ea.timeout, ea.interval))
}

func (ea *elementAccessTimeout) KeyDown(key uint16) Result {
	return newResult(doWithRetryNV(func() error {
		return ea.e.input.KeyDown(key)
	}, ea.timeout, ea.interval))
}

func (ea *elementAccessTimeout) KeyUp(key uint16) Result {
	return newResult(doWithRetryNV(func() error {
		return ea.e.input.KeyUp(key)
	}, ea.timeout, ea.interval))
}

type positionalAccess struct {
	e        *Element
	timeout  time.Duration
	interval time.Duration
	point    Point
	err      error
}

func (pa *positionalAccess) Click(button int) Result {
	log.Println("Clicking element", pa.e.scope, "at", pa.point)
	return newResult(doWithRetryNV(func() error {
		err := pa.e.input.MoveMouse(pa.point, true)
		if err != nil {
			return err
		}
		return pa.e.input.Click(native.MouseButton(button))
	}, pa.timeout, pa.interval))
}

func getDeadline(timeout time.Duration) time.Time {
	return time.Now().Add(timeout)
}

func doWithRetry[T any](f func() (T, error), timeout, interval time.Duration) (T, error) {
	var rv T
	var err error
	deadline := getDeadline(timeout)
	for {
		rv, err = f()
		if err == nil || time.Now().After(deadline) {
			break
		}

		time.Sleep(interval)
	}

	return rv, err
}

func doWithRetryNV(f func() error, timeout, interval time.Duration) error {
	var err error
	deadline := getDeadline(timeout)
	for {
		err = f()
		if err == nil || time.Now().After(deadline) {
			break
		}

		time.Sleep(interval)
	}

	return err
}

func (ea *elementAccessTimeout) GetChild(relativePath string) ValueResult[*Element] {
	absolutePath := path.Join(ea.e.scope, relativePath)
	log.Println("Get element at", absolutePath)

	return newValueResult(doWithRetry(func() (*Element, error) {
		node, err := ea.e.backend.GetElement(absolutePath)
		if err != nil {
			return nil, err
		}
		return &Element{
			scope:   absolutePath,
			backend: ea.e.backend,
			input:   ea.e.input,
			node:    node,
		}, nil
	}, ea.timeout, ea.interval))
}

func (ea *elementAccessTimeout) GetChildAttributes(relativePath string) ValueResult[[]*Attribute] {
	absolutePath := path.Join(ea.e.scope, relativePath)
	log.Println("Get attributes of", absolutePath)

	return newValueResult(doWithRetry(func() ([]*Attribute, error) {
		a, err := ea.e.backend.GetAttributes(absolutePath)
		if err != nil {
			return nil, err
		}
		return a, nil
	}, ea.timeout, ea.interval))
}

func (ea *elementAccessTimeout) GetAttributes() ValueResult[[]*Attribute] {
	log.Println("Get attributes of", ea.e.scope)
	return newValueResult(doWithRetry(func() ([]*Attribute, error) {
		a, err := ea.e.backend.GetAttributes(ea.e.scope)
		if err != nil {
			return nil, err
		}
		return a, nil
	}, ea.timeout, ea.interval))
}
