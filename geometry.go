package inspectux

type Point struct {
	X uint32
	Y uint32
}

func (p Point) Add(p2 Point) Point {
	return Point{
		X: p.X + p2.X,
		Y: p.Y + p2.Y,
	}
}

type Rect struct {
	X uint32
	Y uint32
	W uint32
	H uint32
}

func (r Rect) Center() Point {
	return Point{
		X: r.X + r.W/2,
		Y: r.Y + r.H/2,
	}
}

func (r Rect) Min() Point {
	return Point{
		X: r.X,
		Y: r.Y,
	}
}

func (r Rect) Max() Point {
	return Point{
		X: r.X + r.W,
		Y: r.Y + r.H,
	}
}
