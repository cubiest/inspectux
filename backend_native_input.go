package inspectux

import (
	"context"
	"fmt"

	"gitlab.com/cubiest/inspectux/native"
)

var (
	// Ensure UinputBackend implements Input
	_ Input = &NativeInputBackend{}
)

type NativeInputBackend struct {
	remote native.InputRemote
}

func (u *NativeInputBackend) Check() error {
	// TODO: Check if API Version meets minimal version
	_, err := u.remote.GetAPIVersion(context.Background())
	if err != nil {

		return fmt.Errorf("error checking input capabilities: %w", err)
	}

	return nil
}

// Click implements Input.
func (u *NativeInputBackend) Click(button native.MouseButton) error {
	return u.remote.Click(context.Background(), uint8(button))
}

// Kind implements Input.
func (u *NativeInputBackend) Kind() string {
	// TODO: This should be provided via dbus
	return "uinput"
}

// MouseDown implements Input.
func (u *NativeInputBackend) MouseDown(button native.MouseButton) error {
	panic("unimplemented")
}

// MouseUp implements Input.
func (u *NativeInputBackend) MouseUp(button native.MouseButton) error {
	panic("unimplemented")
}

// MoveMouse implements Input.
func (u *NativeInputBackend) MoveMouse(pos Point, absolute bool) error {
	return u.remote.MoveMouse(context.Background(), int32(pos.X), int32(pos.Y), absolute)
}

// KeyDown implements Input.
func (u *NativeInputBackend) KeyDown(key uint16) error {
	return u.remote.KeyDown(context.Background(), key)
}

// KeyUp implements Input.
func (u *NativeInputBackend) KeyUp(key uint16) error {
	return u.remote.KeyUp(context.Background(), key)
}

// Type implements Input.
func (u *NativeInputBackend) Type(text string) error {
	return u.remote.Type(context.Background(), text)
}
