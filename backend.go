package inspectux

import "gitlab.com/cubiest/inspectux/native"

type BackendNode struct {
	Path  string
	Name  string
	Class string
}

type Attribute struct {
	Name  string
	Type  string
	Value interface{}
}

type BackendSetup[T any] interface {
	// ConfigureEnv returns env vars to be set when starting an application for this backend.
	// May return an empty slice, if nothing needs to be configured.
	ConfigureEnv() []string
	// AttachToPID instructs the backend to connect to a process with the given PID.
	// Depending on the backend, this may be ignored.
	// Returns the backend to be used at runtime.
	AttachToPID(pid int) (T, error)
}

type Backend interface {
	Kind() string

	GetChildren(elementPath string) ([]*BackendNode, error)
	GetElement(elementPath string) (*BackendNode, error)
	GetAttributes(elementPath string) ([]*Attribute, error)
	GetRect(elementPath string, global bool) (*Rect, error)
}

type Input interface {
	Kind() string

	Click(button native.MouseButton) error
	MoveMouse(pos Point, absolute bool) error
	MouseDown(button native.MouseButton) error
	MouseUp(button native.MouseButton) error

	Type(text string) error
	KeyDown(key uint16) error
	KeyUp(key uint16) error
}
