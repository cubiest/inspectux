package inspectux

import (
	"fmt"
	"log"

	"github.com/godbus/dbus/v5"
	"gitlab.com/cubiest/inspectux/native"
)

var (
	// Ensure UinputBackendSetup can set up an Input backend
	_ BackendSetup[Input] = &UinputBackendSetup{}
)

type UinputBackendSetup struct{}

func NewUinputBackend() *UinputBackendSetup {
	return &UinputBackendSetup{}
}

// ConfigureEnv implements BackendSetup.
func (u *UinputBackendSetup) ConfigureEnv() []string {
	// Nothing to configure
	return []string{}
}

// AttachToPID implements BackendSetup.
func (u *UinputBackendSetup) AttachToPID(pid int) (Input, error) {
	log.Println("Connection input backend")

	// TODO: Attempt to start daemon, if connection fails
	// TODO: Try system bus if session bus has no matching interface

	sessionBus, err := dbus.SessionBus()
	if err != nil {
		return nil, fmt.Errorf("error connecting to session bus: %w", err)
	}

	backend := &NativeInputBackend{
		remote: native.NewNativeInput(sessionBus.Object(native.ServiceNameNativeInput, dbus.ObjectPath("/"))),
	}

	err = backend.Check()
	if err != nil {
		return nil, err
	}

	log.Println("Input backend connected")
	return backend, nil
}
