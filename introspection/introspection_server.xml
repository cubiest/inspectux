<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of InspecTux.

Copyright © 2024  Cubiest GmbH
Copyright © 2024  Oliver Kahrmann
SPDX-License-Identifier: LGPL-3.0-or-later

InspecTux is free software: you can redistribute it
and/or modify it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

InspecTux is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with InspecTux.
If not, see <https://www.gnu.org/licenses/>.
-->
<!DOCTYPE node PUBLIC "-//freedesktop//DTD D-BUS Object Introspection 1.0//EN" "http://www.freedesktop.org/standards/dbus/1.0/introspect.dtd">
<node name="/">
	<interface name="com.cubiest.inspectux.introspectionserver">
		<method name="GetChildren">
			<arg type="s" name="elementPath" direction="in" />
			<arg type='(ya(ss))' name='children' direction='out' />
			<annotation name="org.gtk.GDBus.DocString"
				value="Get the children of an element at the given (absolute) path. Returns a return code (byte) and an array of name+class. RC 0 means success, RC 1 means the element was not found, RC 2 means there was error obtaining the element." />
		</method>
		<method name="GetElement">
			<arg type="s" name="elementPath" direction="in" />
			<arg type='(yss)' name='element' direction='out' />
			<annotation name="org.gtk.GDBus.DocString"
				value="Get a single element at the given (absolute) path. Returns a return code (byte) and name+class of the element. Can be used to check existence of an element without loading all attributes. RC 0 means success, RC 1 means the element was not found, RC 2 means there was error obtaining the element." />
		</method>
		<method name="GetAttributes">
			<arg type="s" name="elementPath" direction="in" />
			<arg type='(ya(sv))' name='attributes' direction='out' />
			<annotation name="org.gtk.GDBus.DocString"
				value="Get the attributes of an element at the given (absolute) path. Returns a return code (byte) and an array of key+value, value may be anything. RC 0 means success, RC 1 means the element was not found, RC 2 means there was error obtaining the element." />
		</method>
		<method name="GetRect">
			<arg type="s" name="elementPath" direction="in" />
			<arg type="b" name="global" direction="in" />
			<arg type='(yuuuu)' name='attributes' direction='out' />
			<annotation name="org.gtk.GDBus.DocString"
				value="Get the rect of an element, if it is a visual element. Returns a return code (byte) and 4 unsigned integers for x, y, w and h. RC 0 means success, RC 1 means the element was not found, RC 2 means there was error obtaining the element, RC 3 means the element can't provide the necessary values (not a visual element)." />
		</method>

		<!-- TODO: These should be properties -->
		<method name="GetAPIVersion">
			<arg type='s' name='version' direction='out' />
			<annotation name="org.gtk.GDBus.DocString"
				value="Get the version of the API this introspection server uses. This describes available methods in DBus, not the available attributes of any objects." />
		</method>
		<method name="GetCapabilities">
			<arg type='as' name='capabilities' direction='out' />
			<annotation name="org.gtk.GDBus.DocString"
				value="Get the capabilities of this introspection server. Capabilities are server-defined and may be used to check if a server supports a required feature." />
		</method>
	</interface>
</node>
