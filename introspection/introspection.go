package introspection

import (
	"context"

	"github.com/godbus/dbus/v5"
)

const (
	ServiceNamePrefix = "com.cubiest.inspectux.introspectionserver.pid"
)

type IntrospectionserverRemote interface {
	GetChildren(ctx context.Context, elementPath string) (children struct {
		V0 byte
		V1 []struct {
			V0 string
			V1 string
		}
	}, err error)
	GetElement(ctx context.Context, elementPath string) (element struct {
		V0 byte
		V1 string
		V2 string
	}, err error)
	GetAttributes(ctx context.Context, elementPath string) (attributes struct {
		V0 byte
		V1 []struct {
			V0 string
			V1 dbus.Variant
		}
	}, err error)
	GetRect(ctx context.Context, elementPath string, global bool) (attributes struct {
		V0 byte
		V1 uint32
		V2 uint32
		V3 uint32
		V4 uint32
	}, err error)

	GetAPIVersion(ctx context.Context) (version string, err error)
	GetCapabilities(ctx context.Context) (capabilities []string, err error)
}
