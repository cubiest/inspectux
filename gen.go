package inspectux

//go:generate go run github.com/amenzhinsky/dbus-codegen-go -output introspection/introspection_server.go -package introspection -client-only -gofmt -camelize -prefix com.cubiest.inspectux introspection/introspection_server.xml
//go:generate goimports -w introspection/introspection_server.go
//go:generate go run github.com/amenzhinsky/dbus-codegen-go -output native/native_input_daemon.go -package native -client-only -gofmt -camelize -prefix com.cubiest.inspectux native/native_input_daemon.xml
//go:generate goimports -w native/native_input_daemon.go
